package com.alxconnect.recycleapp.dao;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.alxconnect.recycleapp.model.Usuario;

import java.net.PortUnreachableException;

@Database(entities = {Usuario.class}, version = 1)
public abstract class UsuarioDataBase extends RoomDatabase {

    private static final String DB_NAME = "usuario.db";
    private static volatile UsuarioDataBase instance;

    public static UsuarioDataBase getInstance(Context context){
        if(instance == null){
            instance = create(context);
        }
        return instance;
    }

    private static UsuarioDataBase create(Context context){
        return Room.databaseBuilder(context, UsuarioDataBase.class, DB_NAME).build();
    }

    public abstract UsuarioDAO getDao();


}
