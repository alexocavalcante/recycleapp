package com.alxconnect.recycleapp.dao;

import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.alxconnect.recycleapp.model.Usuario;

import java.util.List;

public interface UsuarioDAO {

    @Query("SELECT * FROM usuario")
    List<Usuario> getAllUsuarios();

    @Query("SELECT * FROM usuario WHERE id LIKE :id")
    Usuario getUsuarioById(String id);

    @Insert
    void insert(Usuario... usuarios);

    @Update
    void update(Usuario... usuarios);

    @Delete
    void delete(Usuario... usuarios);

}
